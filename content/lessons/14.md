---
title: "Middle game"
date: 2019-03-31T10:05:17+02:00
draft: false
cookieSetting: "14"
puzzles:
- id: pzl1
  target: 14-1
  text: Middle game 1
- id: pzl2
  target: 14-2
  text: Middle game 2
- id: pzl3
  target: 14-3
  text: Middle game 3
returnTo: "lessons/15"
section: "index.html#strategies"
---

# | The Middle Game
## How to not get completely lost among all the possibilities.

> This will be a tough chapter, full of different concepts. There is no other way. We can condense the opening into several common options (that are considered optimal). The endgame follows quite a logical sequence of moves. But the middle game? It is where the majority of the moves happens and where you have to fill the huge open space. There is just so many options...

**Let's say you and your opponent split the corners somehow, then took some of the sides maybe... Now what?**

There is not a simple answer. It all depends on the context. Let's try to imagine some possible situations and their potential answers.

You do not really see the move you want to play immediately. You have to do a lot of mental reductions first. You reduce the whole board to a several important areas, then from these areas you chose the best move you can find. Imagine the middle game as a series of questions you need to answer in the right order. There will always be exceptions, but the basic questions you should ask yourself might look like this.

{{< rule >}}
	{{< blackBig "1. " >}}Do I have a {{< black "weak group?" >}} 
{{< /rule >}}
If you do, you should protect it. Otherwise your opponent will attack and either kill your group outright (which can even be game ending), or profit heavily by making points, while you struggle to find two eyes (or connection) making no points in the process. 

{{< extra "Weak-groups" "What does a weak group look like?" >}}
It is a group that you do not see a safe way of creating two eyes for, while being close to surrounded by opponent's stones. Another possibile weak group is a group that can be cut apart from the rest.
{{< /extra >}}

{{< rule >}}
	{{< blackBig "2. " >}}Does my opponent have a {{< black "weak group?" >}}
{{< /rule >}}
If so, it should be good to pressure it! Do not be reckless. You do not necessarily have to kill it, even "just" attacking it can give you lots of "free" stuff. Going too overboard with the "attack mode" can often lead to trouble as the attacking sides creates so many weaknesses that once the attack fails the opponent just takes everything apart.

{{< rule >}}
	{{< blackBig "3. " >}}Can I claim (or threaten to claim) a huge chunk of {{< black "territory" >}} or prevent my opponent from doing so?
{{< /rule >}} 
If there are no weak groups, it is often best to just go for a nice big part of territory, or prevent your opponent from doing the same. Or even better do both at once.<br><br><br><br>

**After asking the above questions, you should at least reduce the number of possibilities to a few general areas where to play. Remember those areas and ask further:**

{{< rule >}}
	{{< blackBig "1. " >}}Can I safely {{< black "keep sente" >}} in either of these scenarios?
{{< /rule >}}
If you can, that's perfect. Play in this area first, and then you can play in the other one(s) as well.
  
{{< extra "Sente-gote" "Sente vs gote" >}}
	<b>Sente</b> - can be loosely translated as initiative. When you have sente, it means you do not feel obligated to answer your opponent's last move, and thus can play anywhere else on the board. Often we also say to "play a move in sente", which means that we are hoping our move will force the opponent to answer locally, and we will keep sente.
	<br><br>
	<b>Gote</b> - is the opposite. Meaning we feel obligated to answer opponent's last move and hence opponent will keep the ability to freely choose where to play next.
	<br><br>
	It is an important concept, and having the chance to freely chose where the game will develop next is certainly an advantage not to be overlooked.
{{< /extra >}}


{{< rule >}}
	{{< blackBig "2. " >}}Who is {{< black "winning?" >}}
{{< /rule >}} 
A lot can depend on the answer. Who has a better potential to win this game right now? If it is your opponent, it's time to try some sort of an attack, not just keep protecting. If you are the one with better prospects of winning, do not take it as an excuse to play slack, but stay away from crazy attacks and instead make sure your territory and groups are safe and solid.

{{< rule >}}
	{{< blackBig "3. " >}}Which of these is {{< black "worth more?" >}}
{{< /rule >}}
If you have to give up sente (and there is no shame in doing so), you have to choose the move that is worth most. That can be very hard to estimate! You do not only judge the immediate territory gain, but also the future prospects and influence on the rest of the board, endgame potential... We mostly just eyeball it and admittedely it comes with experience, but try to at least think about it. 
 
{{< tsumego >}}


 

