---
title: "What to Do in the Opening"
date: 2019-03-31T10:05:17+02:00
draft: false
cookieSetting: "13"
puzzles:
- id: pzl1
  target: 13-1
  text: Corners
- id: pzl2
  target: 13-2
  text: Why corners
- id: pzl3
  target: 13-3
  text: Territory vs influence
- id: pzl4
  target: 13-4
  text: Enclosures
returnTo: "lessons/14"
section: "index.html#strategies"
---

# | What To Do in the Opening
## How do you even start on such a huge board?

> From now on we will be talking about the 19x19 board. If you are still having fun on 9x9 there is no rush to switch. You can come back whenever you want. 

**We do not know much but are pretty sure you should start near the corners.**

Now that does not mean that if you start near a corner and your opponent doesn't you win automatically but it should start you in the right direction. Generally speaking, we first take the **corners**, then the **sides** and lastly, the **center**. In Go there is an exception for everything but it is very good to keep this general direction in mind (we call these moves that have a potential to surround many points "big"). 

One possible exception to this approach is when there is a stone (be it yours or your opponent's) in trouble. If the stone is yours, you save or help it. If the stone is your opponent's you attack it. We say **urgent before big**.
{{< sgf >}}